# Insertar datos en MongoDB
# -*- coding: utf-8 -*-

from pymongo import MongoClient
import csv
import urllib2
import json
import StringIO
from bson import json_util

# CONEXION CON MONGO ATLAS
def mongo_connect():
    uri_mongo = "mongodb://user:password@proyecto1-shard-00-00-olk1t.gcp.mongodb.net:27017,proyecto1-shard-00-01-olk1t.gcp.mongodb.net:27017,proyecto1-shard-00-02-olk1t.gcp.mongodb.net:27017/test?ssl=true&replicaSet=proyecto1-shard-0&authSource=admin&retryWrites=true&w=majority" #Cadena MONGO ATLAS ---------> MODIFICAR
    client = MongoClient(uri_mongo)
    client.test
    return client


def read_csv(csv_path,client):

    # DETERMINAR DB Y COLECCIÓN
    db = client.Mongo  # database ----------> MODIFICAR
    col = db.Prueba  # collection ----------> MODIFICAR

    # JSON FILE
    '''
    archivo = urllib.urlopen(csv_path) #open file in url
    json_file=json.loads(archivo.read())
    ls_rows={}
    for i in json_file:
        col.insert_one(i) #insert in client.Mongo.Prueba
    print ('Se agregaron los datos correctamente a Mongo.')    
    '''

    # CSV FILE
    #'''
    ls_rows = {} #create dictionary
    file = urllib2.urlopen(csv_path) #open file in url
    ls_csv =  csv.DictReader(file,delimiter=';') #separate data

    for i in ls_csv:
        ls_rows.update(i) #add data to the dictionary
        col.insert_one(i)
    return ls_rows
    print ('Se agregaron los datos correctamente a Mongo.')
    #'''
    

if __name__ == "__main__": #se define main

    #SOLO 8 DATOS
    url_file='http://apigobiernoabiertortod.valencia.es/apirtod/rest/datasets/intensidad_espiras.csv'

    #JSON FILE 96 DATOS
    #url_file='https://opendata-ajuntament.barcelona.cat/data/dataset/671c46e9-5b85-4e63-8c97-088a2b907cd5/resource/7a3d5380-f79a-424e-be62-dd078efcb40a/download/2019_censcomercialbcn_class_act.json'

    #MAS DE 2000 DATOS
    #url_file='http://mapas.valencia.es/WebsMunicipales/uploads/atmosferica/1A.csv' #file

    #LLAMAR FUNCIONES
    client= mongo_connect()
    ls_csv = read_csv(url_file,client)


