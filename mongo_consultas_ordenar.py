# Insertar datos en MongoDB
# -*- coding: utf-8 -*-

from pymongo import MongoClient
import csv
import urllib2
import json

# CONEXION CON MONGO ATLAS
def mongo_connect():
    uri_mongo = "mongodb+srv://user:password@proyecto1-olk1t.gcp.mongodb.net/test?retryWrites=true&w=majority" #Cadena MONGO ATLAS ---------> MODIFICAR
    client = MongoClient(uri_mongo)
    client.test
    return client

# CONSULTA Y ORDENA. ENTREGA RESULTADOS TOP
def ordenar_atributo(col):
    print ('CONSULTAS')
    print ('')
    buscar_atributo= input('Ingrese atributo a consultar >>> ') #debe de ser elemento de diccionario
    #valor= input('Ingrese un valor guía >>> ')
    #valor_string=str(valor) #convertir valor númerico a string
    data=col.find({})
    lista=[]
    cont=0
    for i in data:
        if (cont<3):
            lista.append(i[buscar_atributo])
            cont=cont+1
        else:
            break
    ordenada = sorted(lista)
    for i in ordenada:
        data2=col.find({buscar_atributo:i}).limit(1)
        for j in data2:
            print(buscar_atributo,i,j)

if __name__ == "__main__":
    print('>>>>> Start <<<<<')
    print('')
    
    #DEFINIR COLECCIÓN Y BASE DE DATOS
    
    # Functions
    client = mongo_connect()
    
    db = client.Mongo
    col = db.Prueba
          
    # get data
    ordenar_atributo(col)

    print('')
    print('>>>>> Finish <<<<<')
